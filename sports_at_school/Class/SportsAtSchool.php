<?php

class SportsAtSchool {

	private $_db;
	
	private $_nbStudentA;
	private $_nbStudentB;
	private $_nbStudentC;
	
	private $nbSportStudentA;
	private $nbSportStudentB;
	private $nbSportStudentC;
	
	public function __construct($db){
	$this->setDb($db);
	
	//--------------------------------------------------Ecoles de 50 à 200 éléves--------------------------------------------------//
	
	$this->_nbStudentA=rand(50,200);
	$this->_nbStudentB=rand(50,200);
	$this->_nbStudentC=rand(50,200);
	
	//--------------------------------------Calcul aleatoire du nombre d'éléve a répartir entre chaque sport--------------------------------------//
	
	$this->_nbSportStudentA=round((rand(0,300))/100*$this->_nbStudentA);
	$this->_nbSportStudentB=round((rand(0,300))/100*$this->_nbStudentB);
	$this->_nbSportStudentC=round((rand(0,300))/100*$this->_nbStudentC);
	}

	public function setDb(PDO $dbh){
	$this->_db=$dbh;
	}
	
	public function setRandStudent(){
	
	//--------------------------------------------Enregistrement du nombre d'éléve par école---------------------------------------//
	
	$reqA=$this->_db->prepare('UPDATE schools SET nb_student=:nbStudentA WHERE name="A"');
	$reqB=$this->_db->prepare('UPDATE schools SET nb_student=:nbStudentB WHERE name="B"');
	$reqC=$this->_db->prepare('UPDATE schools SET nb_student=:nbStudentC WHERE name="C"');
			
			$reqA->execute(array('nbStudentA'=>$this->_nbStudentA));
			$reqB->execute(array('nbStudentB'=>$this->_nbStudentB));
			$reqC->execute(array('nbStudentC'=>$this->_nbStudentC));				
	}
	
	public function setDistribSports($school){
	
	$nbSportStudentA=$this->_nbSportStudentA;
	$nbSportStudentB=$this->_nbSportStudentB;
	$nbSportStudentC=$this->_nbSportStudentC;
	
	echo (rand(0,300)/100);
	//--------------------------------------------Tirage aléatoire du nombre d'éléve par sport---------------------------------------//
	 do {
     $a = range(0,${'nbSportStudent'.$school});
     shuffle($a);
     $b = array_slice($a, 0,5);
	 } while(array_sum($b) != ${'nbSportStudent'.$school});
 
	//--------------------------------------------Enregistrement du nombre d'éléve par sport---------------------------------------//
	
	$req0=$this->_db->prepare('UPDATE sports SET nb_student=:nbStudent0 WHERE name_school=:school AND sport="football" ');
	$req1=$this->_db->prepare('UPDATE sports SET nb_student=:nbStudent1 WHERE name_school=:school AND sport="boxe" ');
	$req2=$this->_db->prepare('UPDATE sports SET nb_student=:nbStudent2 WHERE name_school=:school AND sport="judo" ');
	$req3=$this->_db->prepare('UPDATE sports SET nb_student=:nbStudent3 WHERE name_school=:school AND sport="natation" ');
	$req4=$this->_db->prepare('UPDATE sports SET nb_student=:nbStudent4 WHERE name_school=:school AND sport="cyclisme" ');
			
			for($i=0;$i<5;$i++){
			${'req'.$i}->execute(array('nbStudent'.$i=>$b[$i],
											       'school'=>$school));
			}			
	}
	
	public function getSumStudent($school){
	
	//--------------------------------------------Affichage du nombre d'éléve par école---------------------------------------//
	
			$req=$this->_db->prepare('SELECT name, nb_student FROM schools WHERE name=:name');
			$req->execute(array(
			'name'=>$school,
			));
			
			while($data=$req->fetch()){
			echo "</br>Pour l'école ".$data['name'].", il y a ".$data['nb_student']." éléves.</br>";
			}
	}
	
	public function getSumStudentSports($school){
	
	//--------------------------------------------Affichage du nombre d'éléve pratiquant un sport par école---------------------------------------//	
	
			$req=$this->_db->prepare('SELECT SUM(nb_student) AS nbStudent FROM sports WHERE name_school=:name');
			$req->execute(array(
			'name'=>$school,
			));
			
			while($data=$req->fetch()){
			echo "La somme des éléves sur la liste des sports est de ".$data['nbStudent'].".</br>";
			}		
	}
	
	public function getCountSports($school){
	
	//--------------------------------------------Affichage du nombre de sport pratiqué par école---------------------------------------//	
	
			$req=$this->_db->prepare('SELECT COUNT(sport) AS countSport FROM sports WHERE name_school=:name AND nb_student>0');
			$req->execute(array(
			'name'=>$school,
			));
			
			while($data=$req->fetch()){
			echo "Le nombre de sport pratiqué est de ".$data['countSport'].".</br>";
			}		
	}
	
	public function getListSports($school){
	
	//--------------------------------------------Affichage du nombre de sport pratiqué par école par liste décroissante---------------------------------------//	
	
			$req=$this->_db->prepare('SELECT sport,nb_student FROM sports WHERE name_school=:name ORDER BY nb_student DESC');
			$req->execute(array(
			'name'=>$school,
			));
			
			while($data=$req->fetch()){
			echo "Pour le sport ".$data['sport'].", le nombre de pratiquant est de ".$data['nb_student'].".</br>";
			}		
	}
}