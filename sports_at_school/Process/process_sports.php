<?php
require('Class/SportsAtSchool.php');

$db=new PDO('mysql:host=localhost;dbname=sports_at_school','root','');

if(isset($_POST['submitA'])){
	$sport=new SportsAtSchool($db);

	$sport->getSumStudent("A");
	$sport->getSumStudentSports("A");
	$sport->getCountSports("A");
	$sport->getListSports("A");
	$sport->setDistribSports("A");
}

if(isset($_POST['submitB'])){
	$sport=new SportsAtSchool($db);

	$sport->getSumStudent("B");
	$sport->getSumStudentSports("B");
	$sport->getCountSports("B");
	$sport->getListSports("B");
	$sport->setDistribSports("B");
}

if(isset($_POST['submitC'])){
	$sport=new SportsAtSchool($db);

	$sport->getSumStudent("C");
	$sport->getSumStudentSports("C");
	$sport->getCountSports("C");
	$sport->getListSports("C");
	$sport->setDistribSports("C");
}

if(isset($_POST['submitRand'])){
	$sport=new SportsAtSchool($db);

	$sport->setRandStudent();
}