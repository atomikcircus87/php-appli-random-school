-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Jeu 26 Février 2015 à 15:20
-- Version du serveur :  5.6.17
-- Version de PHP :  5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `sports_at_school`
--

-- --------------------------------------------------------

--
-- Structure de la table `schools`
--

CREATE TABLE IF NOT EXISTS `schools` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(3) NOT NULL,
  `nb_student` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `schools`
--

INSERT INTO `schools` (`id`, `name`, `nb_student`) VALUES
(1, 'A', 172),
(2, 'B', 110),
(3, 'C', 128);

-- --------------------------------------------------------

--
-- Structure de la table `sports`
--

CREATE TABLE IF NOT EXISTS `sports` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `sport` varchar(23) NOT NULL,
  `name_school` varchar(3) NOT NULL,
  `nb_student` int(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Contenu de la table `sports`
--

INSERT INTO `sports` (`id`, `sport`, `name_school`, `nb_student`) VALUES
(1, 'Football', 'A', 5),
(2, 'Natation', 'A', 4),
(3, 'Judo', 'A', 30),
(4, 'Boxe', 'A', 31),
(5, 'Cyclisme', 'A', 22),
(6, 'Football', 'B', 29),
(7, 'Natation', 'B', 134),
(8, 'Judo', 'B', 8),
(9, 'Boxe', 'B', 38),
(10, 'Cyclisme', 'B', 194),
(11, 'Football', 'C', 122),
(12, 'Natation', 'C', 158),
(13, 'Judo', 'C', 9),
(14, 'Boxe', 'C', 6),
(15, 'Cyclisme', 'C', 24);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
